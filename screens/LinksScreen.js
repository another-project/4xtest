import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput
} from 'react-native';
import { Button, Portal, Dialog } from 'react-native-paper';

import { MonoText } from '../components/StyledText';

export default class LinksScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        kata:"",
        modalVisible:false,
        hasil:"false"
    };
  }

  _hideDialog = () => this.setState({ modalVisible: false });  

  _cekPalindrom = async () =>{
      var str = this.state.kata
      var re = '/[\W_]/g';
      var lowRegStr = str.toLowerCase().replace(re, '');
      var reverseStr = lowRegStr.split('').reverse().join(''); 
      if(reverseStr === lowRegStr){
        this.setState({hasil : "true"})
      }
      else{
        this.setState({hasil : "false"})
      }
      this.setState({modalVisible : true})

      return true;
  }

  render(){
  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}>
        <View style={styles.welcomeContainer}>
          <Image
            source={
              __DEV__
                ? require('../assets/images/logo.png')
                : require('../assets/images/logo.png')
            }
            style={styles.welcomeImage}
          />
        </View>

        <View style={styles.getStartedContainer}>

          <Text style={styles.getStartedText}>Test Kedua</Text>

          <View
            style={[styles.codeHighlightContainer, styles.homeScreenFilename]}>
            <MonoText>Cek Input Sebuah Angka Palindrom</MonoText>
          </View>
        </View>

        <View style={styles.helpContainer}>
                <Text>Masukkan Sebuah Angka</Text>
                <TextInput keyboardType='default' onChangeText={(value) => this.setState({ kata: value })}
                style={{borderRadius: 3, borderWidth: 1, borderColor: 'rgba(0, 0, 0, 0.25)', marginTop: 5, height: 35, alignSelf: 'stretch', padding: 5, marginBottom: 15, backgroundColor: '#fff'}} value={this.state.kata} keyboardType={'numeric'}  />  
        </View>

        <View style={{flex: 0.5}}>
                    <Button style={{backgroundColor: '#C62F4C', marginRight: 3, flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} color="#fff" 
                      onPress={() => this._cekPalindrom()}>
                        <Text style={{fontSize: 13, color: '#fff'}}> Cek Sekarang</Text>
                    </Button>
        </View>

        <Portal>
          <Dialog visible={this.state.modalVisible} onDismiss={this._hideDialog} style={{paddingVertical: 20, borderRadius: 20}}>
            <Dialog.Content>
              <View style={{justifyContent: 'center', flexDirection: 'row'}}>
                <Text>{this.state.hasil}</Text>
              </View> 
            </Dialog.Content>
            <TouchableOpacity onPress={this._hideDialog} style={{position: 'absolute', top: 5, right: 6}}><Text>Close</Text></TouchableOpacity> 
          </Dialog>
        </Portal> 

      </ScrollView>


    </View>
  );
}
}

LinksScreen.navigationOptions = {
  title: 'Test 2',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
