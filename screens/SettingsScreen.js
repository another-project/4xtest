import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput
} from 'react-native';
import { Button } from 'react-native-paper';
import { Notifications } from 'expo'; 
import * as Permissions from 'expo-permissions';

import { MonoText } from '../components/StyledText';

export default class SettingsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        palindrom:"",
        modalVisible:false,
        token:""
    };
  }

  componentDidMount(){
    this.registerForPushNotificationsAsync();  
  }

  async registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus; 

    if (existingStatus !== 'granted') {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
    } 

    if (finalStatus !== 'granted') {
    return;
    } 
    tokens = await Notifications.getExpoPushTokenAsync();
    this.setState({token:tokens})
    console.log(tokens)
  }   

  _getNotif = async () =>{
    const expo_token = this.state.token; 
    console.log(expo_token)
    if(expo_token){
      fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: new Headers({ 
            'host': 'exp.host',
            'accept': 'application/json',
            'accept-encoding': 'gzip, deflate',
            'content-type': 'application/json'
        }),
        body: JSON.stringify({
            "to": expo_token,
            "title": "Pengumuman",
            "body": "Berhasil Push Notif"
        }),
      }).then((response) => response.json())
      .then((responseJson) => {  
        console.log(responseJson)
    });
    }else{
      alert('Tidak terhubung')
    } 
   
  }

  render(){
  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}>
        <View style={styles.welcomeContainer}>
          <Image
            source={
              __DEV__
                ? require('../assets/images/logo.png')
                : require('../assets/images/logo.png')
            }
            style={styles.welcomeImage}
          />
        </View>

        <View style={styles.getStartedContainer}>

          <Text style={styles.getStartedText}>Test Ketiga</Text>

          <View
            style={[styles.codeHighlightContainer, styles.homeScreenFilename]}>
            <MonoText>Klik Untuk Dapat Push Notif Ya :)</MonoText>
          </View>
        </View>

        <View style={{flex: 0.5}}>
              <Button style={{backgroundColor: '#C62F4C', marginRight: 3, flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} color="#fff" 
                onPress={() => this._getNotif()}>
                <Text style={{fontSize: 13, color: '#fff'}}> Get Now</Text>
              </Button>
        </View>
      </ScrollView>
    </View>
  );
}
}

SettingsScreen.navigationOptions = {
  title: 'Test 3',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
